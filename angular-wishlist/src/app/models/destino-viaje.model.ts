import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	selected: boolean;
	servicios: string[];
	id = uuid();
	constructor(public nombre: string, public imagenUrl: string) {
		this.servicios = ['Desayuno', 'Cena'];
	 }

	isSelected(): boolean {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
}
